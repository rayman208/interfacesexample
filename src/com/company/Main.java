package com.company;

import java.util.ArrayList;

public class Main {

    public static void main(String[] args)
    {
        ArrayList<IMovable> movables = new ArrayList<>();

        movables.add(new Car());
        movables.add(new Car());
        movables.add(new Man());
        movables.add(new Plane());

        for (int i = 0; i < movables.size(); i++)
        {
            movables.get(i).move();
        }
    }
}
