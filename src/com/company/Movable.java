package com.company;

public abstract class Movable
{
    abstract void move();
}
